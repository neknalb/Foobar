//
//  FoobarApp.swift
//  Foobar
//
//  Created by mblanken on 14.03.24.
//

import SwiftUI

@main
struct FoobarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
